<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'price'             => $this->price,
            'short_description' => $this->short_description,
            'description'       => $this->description,
            'image'             => $this->image,
            'status'            => $this->status,
            'visibility'        => $this->visibility,
            'attributes'        => AttributeResource::collection($this->attributes)
        ];
    }
}
