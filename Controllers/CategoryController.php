<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;

class CategoryController extends Controller
{
	public $category;

	public function __construct(Category $category)
	{
		$this->category = $category;
	}

    public function index()
    {
        return new CategoryCollection(CategoryResource::collection($this->category->all()));
    }

    public function show($id)
    {
    	return new CategoryResource($this->category->findOrFail($id));
    }
}
