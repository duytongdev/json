<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => true,
            'code'   => 200,
            'data'   => [
                'id'   => $this->id,
                'name' => $this->name,
                'products' => ProductResource::collection($this->products)
            ]
        ];
    }
}
