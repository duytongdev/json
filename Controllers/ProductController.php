<?php

namespace App\Http\Controllers\API;

use App\Product;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;

class ProductController extends Controller
{
	public $product;

	public function __construct(Product $product)
	{
		$this->product = $product;
	}

    public function index()
    {
        return new ProductCollection(ProductResource::collection($this->product->all()));
    }

    public function show($id)
    {
    	return new ProductResource($this->product->findOrFail($id));
    }
}
